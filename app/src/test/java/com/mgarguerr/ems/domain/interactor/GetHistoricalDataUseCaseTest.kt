package com.mgarguerr.ems.domain.interactor

import com.mgarguerr.ems.data.repository.EMSDataRepository
import com.mgarguerr.ems.domain.model.EnergyHistoricalData
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetHistoricalDataUseCaseTest {

    @MockK
    private lateinit var emsRepository: EMSDataRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `invoke should return historical data`() = runBlocking {
        val expectedHistoricalData = listOf(mockk<EnergyHistoricalData>())
        val useCase = buildUseCase()
        coEvery { emsRepository.getHistoricalData() } returns expectedHistoricalData

        val result = useCase.invoke()

        assertEquals(result, expectedHistoricalData)
    }

    private fun buildUseCase() = GetHistoricalDataUseCase(emsRepository)
}
