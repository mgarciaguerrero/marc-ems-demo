package com.mgarguerr.ems.domain.interactor

import com.mgarguerr.ems.data.repository.EMSDataRepository
import com.mgarguerr.ems.domain.model.EnergyLiveData
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetLiveDataUseCaseTest {

    @MockK
    private lateinit var emsRepository: EMSDataRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `invoke should return live data`() = runBlocking {
        val expectedLiveData = mockk<EnergyLiveData>()
        val useCase = buildUseCase()
        coEvery { emsRepository.getLiveData() } returns expectedLiveData

        val result = useCase.invoke()

        assertEquals(result, expectedLiveData)
    }

    private fun buildUseCase() = GetLiveDataUseCase(emsRepository)
}
