package com.mgarguerr.ems.presentation.dashboard

import com.mgarguerr.ems.domain.interactor.GetLiveDataUseCase
import com.mgarguerr.ems.fixtures.EnergyLiveDataFixtures
import com.mgarguerr.ems.utils.DispatchersProviderTest
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class DashboardPresenterTest : EnergyLiveDataFixtures {

    @MockK(relaxUnitFun = true)
    private lateinit var view: DashboardViewContract

    @MockK(relaxUnitFun = true)
    private lateinit var getLiveData: GetLiveDataUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `onViewReady should show loading when it is invoked`() {
        val presenter = buildPresenter()

        presenter.onViewReady()

        verify { view.showLoading() }
    }

    @Test
    fun `onViewReady should hide loading when get live data success`() {
        coEvery { getLiveData.invoke() } returns aEnergyDataLive()
        val presenter = buildPresenter()

        presenter.onViewReady()

        verify { view.hideLoading() }
    }

    @Test
    fun `onViewReady should hide loading when get live data fails`() {
        coEvery { getLiveData.invoke() } throws Exception("Boom!")
        val presenter = buildPresenter()

        presenter.onViewReady()

        verify { view.hideLoading() }
    }

    @Test
    fun `onViewReady should show error when get live data fails`() {
        coEvery { getLiveData.invoke() } throws Exception("Boom!")
        val presenter = buildPresenter()

        presenter.onViewReady()

        verify { view.showError() }
    }

    @Test
    fun `onViewReady should show quasar energy when get live data success`() {
        val energyLiveData = aEnergyDataLive()
        coEvery { getLiveData.invoke() } returns energyLiveData
        val presenter = buildPresenter()

        presenter.onViewReady()

        verify { view.showWidgetQuasarsEnergy(energyLiveData.quasarsPower) }
    }

    @Test
    fun `onViewReady should show all live data energy when get live data success`() {
        val energyLiveData = aEnergyDataLive()
        coEvery { getLiveData.invoke() } returns energyLiveData
        val presenter = buildPresenter()

        presenter.onViewReady()

        verify { view.showWidgetAllLiveData(energyLiveData) }
    }

    private fun buildPresenter() =
        DashboardPresenter(view, getLiveData, DispatchersProviderTest(), TestCoroutineScope())
}
