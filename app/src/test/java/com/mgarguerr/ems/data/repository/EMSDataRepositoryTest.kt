package com.mgarguerr.ems.data.repository

import com.mgarguerr.ems.data.datasource.network.EMSNetworkDataSource
import com.mgarguerr.ems.domain.model.EnergyHistoricalData
import com.mgarguerr.ems.domain.model.EnergyLiveData
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class EMSDataRepositoryTest {

    @MockK
    lateinit var network: EMSNetworkDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `getLiveData should return the response from network`() = runBlocking {
        val expectedLiveData = mockk<EnergyLiveData>()
        val repository = buildRepository()
        coEvery { repository.getLiveData() } returns expectedLiveData

        val responseLiveData = repository.getLiveData()

        assertEquals(responseLiveData, expectedLiveData)
    }

    @Test
    fun `getHistoricalData should return the response from network`() = runBlocking {
        val expectedHistoricalData = listOf(mockk<EnergyHistoricalData>())
        val repository = buildRepository()
        coEvery { repository.getHistoricalData() } returns expectedHistoricalData

        val responseLiveData = repository.getHistoricalData()

        assertEquals(responseLiveData, expectedHistoricalData)
    }

    private fun buildRepository() = EMSDataRepository(network)
}
