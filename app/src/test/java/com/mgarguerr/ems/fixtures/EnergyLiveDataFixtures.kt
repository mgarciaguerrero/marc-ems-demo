package com.mgarguerr.ems.fixtures

import com.mgarguerr.ems.domain.model.EnergyLiveData

interface EnergyLiveDataFixtures {

    private companion object {

        private const val A_SOLAR_POWER = 7.827F
        private const val A_QUASARS_POWER = -38.732F
        private const val A_GRID_POWER = 80.475F
        private const val A_BUILDING_DEMAND = 127.033F
        private const val A_SYSTEM_SOC = 48.333F
        private const val A_TOTAL_ENERGY = 960
        private const val A_CURRENT_ENERGY = 464.0F
    }

    fun aEnergyDataLive(
        withSolar: Float = A_SOLAR_POWER,
        withQuasars: Float = A_QUASARS_POWER,
        withGrid: Float = A_GRID_POWER,
        withBuildingDemand: Float = A_BUILDING_DEMAND,
        withSystemSoc: Float = A_SYSTEM_SOC,
        withTotalEnergy: Int = A_TOTAL_ENERGY,
        withCurrentEnergy: Float = A_CURRENT_ENERGY,
    ) = EnergyLiveData(
        solarPower = withSolar,
        quasarsPower = withQuasars,
        gridPower = withGrid,
        buildingDemand = withBuildingDemand,
        systemDoc = withSystemSoc,
        totalEnergy = withTotalEnergy,
        currentEnergy = withCurrentEnergy
    )
}
