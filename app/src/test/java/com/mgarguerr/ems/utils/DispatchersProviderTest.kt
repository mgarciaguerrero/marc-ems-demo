package com.mgarguerr.ems.utils

import com.mgarguerr.ems.presentation.core.executors.Dispatchers
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher

@ExperimentalCoroutinesApi
class DispatchersProviderTest : Dispatchers {

    override val default: CoroutineDispatcher = TestCoroutineDispatcher()

    override val main: CoroutineDispatcher = TestCoroutineDispatcher()

    override val io: CoroutineDispatcher = TestCoroutineDispatcher()
}
