package com.mgarguerr.ems.domain.interactor

interface UseCase<T> {

    suspend fun invoke(): T
}
