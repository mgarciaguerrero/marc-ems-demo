package com.mgarguerr.ems.domain.repository

import com.mgarguerr.ems.domain.model.EnergyHistoricalData
import com.mgarguerr.ems.domain.model.EnergyLiveData

interface EMSRepository {

    suspend fun getLiveData(): EnergyLiveData

    suspend fun getHistoricalData(): List<EnergyHistoricalData>
}
