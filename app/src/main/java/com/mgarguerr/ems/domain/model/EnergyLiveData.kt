package com.mgarguerr.ems.domain.model

data class EnergyLiveData(
    val solarPower: Float,
    val quasarsPower: Float,
    val gridPower: Float,
    val buildingDemand: Float,
    val systemDoc: Float,
    val totalEnergy: Int,
    val currentEnergy: Float
)
