package com.mgarguerr.ems.domain.interactor

import com.mgarguerr.ems.domain.model.EnergyHistoricalData
import com.mgarguerr.ems.domain.repository.EMSRepository

class GetHistoricalDataUseCase(
    private val emsRepository: EMSRepository
) : UseCase<List<EnergyHistoricalData>> {

    override suspend fun invoke(): List<EnergyHistoricalData> {
        return emsRepository.getHistoricalData()
    }
}
