package com.mgarguerr.ems.domain.model

data class EnergyHistoricalData(
    val buildingActivePower: Float,
    val gridActivePower: Float,
    val pvActivePower: Float,
    val quasarsActivePower: Float,
    val timestamp: String,
)
