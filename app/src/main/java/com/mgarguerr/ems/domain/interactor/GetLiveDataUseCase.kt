package com.mgarguerr.ems.domain.interactor

import com.mgarguerr.ems.domain.model.EnergyLiveData
import com.mgarguerr.ems.domain.repository.EMSRepository
import javax.inject.Inject

class GetLiveDataUseCase @Inject constructor(
    private val emsRepository: EMSRepository
) : UseCase<EnergyLiveData> {

    override suspend fun invoke(): EnergyLiveData {
        return emsRepository.getLiveData()
    }
}
