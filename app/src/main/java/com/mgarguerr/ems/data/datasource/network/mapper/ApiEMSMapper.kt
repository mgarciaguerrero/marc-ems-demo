package com.mgarguerr.ems.data.datasource.network.mapper

import com.mgarguerr.ems.data.datasource.network.model.ApiHistoricalDataResponse
import com.mgarguerr.ems.data.datasource.network.model.ApiLiveDataResponse
import com.mgarguerr.ems.domain.model.EnergyHistoricalData
import com.mgarguerr.ems.domain.model.EnergyLiveData
import javax.inject.Inject

class ApiEMSMapper @Inject constructor() {

    fun transform(api: ApiLiveDataResponse) =
        EnergyLiveData(
            solarPower = api.solarPower,
            quasarsPower = api.quasarsPower,
            gridPower = api.gridPower,
            buildingDemand = api.buildingDemand.toFloat(),
            systemDoc = api.systemDoc.toFloat(),
            totalEnergy = api.totalEnergy,
            currentEnergy = api.currentEnergy
        )

    fun transform(api: ApiHistoricalDataResponse) =
        EnergyHistoricalData(
            buildingActivePower = api.buildingActivePower.toFloat(),
            gridActivePower = api.gridActivePower.toFloat(),
            pvActivePower = api.pvActivePower.toFloat(),
            quasarsActivePower = api.quasarsActivePower.toFloat(),
            timestamp = api.timestamp,
        )
}
