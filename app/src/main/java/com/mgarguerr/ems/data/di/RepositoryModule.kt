package com.mgarguerr.ems.data.di

import com.google.gson.Gson
import com.mgarguerr.ems.data.repository.EMSDataRepository
import com.mgarguerr.ems.domain.repository.EMSRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RepositoryModule {

    companion object {

        @Provides
        fun provideGson() = Gson()
    }

    @Binds
    abstract fun bindEMSRepository(emsRepository: EMSDataRepository): EMSRepository
}
