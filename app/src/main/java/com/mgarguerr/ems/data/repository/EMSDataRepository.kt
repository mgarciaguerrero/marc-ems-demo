package com.mgarguerr.ems.data.repository

import com.mgarguerr.ems.data.datasource.network.EMSNetworkDataSource
import com.mgarguerr.ems.domain.repository.EMSRepository
import javax.inject.Inject

class EMSDataRepository @Inject constructor(
    private val network: EMSNetworkDataSource
) : EMSRepository {

    override suspend fun getLiveData() = network.getLiveData()

    override suspend fun getHistoricalData() = network.getHistoricalData()
}
