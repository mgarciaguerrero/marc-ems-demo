package com.mgarguerr.ems.data.datasource.network.model

import com.google.gson.annotations.SerializedName

data class ApiLiveDataResponse(
    @SerializedName("solar_power") val solarPower: Float,
    @SerializedName("quasars_power") val quasarsPower: Float,
    @SerializedName("grid_power") val gridPower: Float,
    @SerializedName("building_demand") val buildingDemand: Double,
    @SerializedName("system_doc") val systemDoc: Double,
    @SerializedName("total_energy") val totalEnergy: Int,
    @SerializedName("current_energy") val currentEnergy: Float
)
