package com.mgarguerr.ems.data.datasource.network

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mgarguerr.ems.data.datasource.network.mapper.ApiEMSMapper
import com.mgarguerr.ems.data.datasource.network.model.ApiHistoricalDataResponse
import com.mgarguerr.ems.data.datasource.network.model.ApiLiveDataResponse
import com.mgarguerr.ems.di.qualifiers.ForApplication
import com.mgarguerr.ems.domain.model.EnergyHistoricalData
import com.mgarguerr.ems.domain.model.EnergyLiveData
import javax.inject.Inject

class EMSNetworkDataSource @Inject constructor(
    @ForApplication private val context: Context,
    private val gson: Gson,
    private val mapper: ApiEMSMapper
) {

    private companion object {

        const val MOCK_LIVE_DATA_RESPONSE = "live_data.json"
        const val MOCK_HISTORICAL_DATA_RESPONSE = "historic_data.json"
    }

    fun getLiveData(): EnergyLiveData {
        return context
            .assets
            .open(MOCK_LIVE_DATA_RESPONSE)
            .reader()
            .readText()
            .let { gson.fromJson(it, ApiLiveDataResponse::class.java) }
            .let { mapper.transform(it) }
    }

    fun getHistoricalData(): List<EnergyHistoricalData> {
        val itemType = object : TypeToken<List<ApiHistoricalDataResponse>>() {}.type
        return context
            .assets
            .open(MOCK_HISTORICAL_DATA_RESPONSE)
            .reader()
            .readText()
            .let { gson.fromJson(it, itemType) as List<ApiHistoricalDataResponse> }
            .map { mapper.transform(it) }
    }
}
