package com.mgarguerr.ems.presentation.dashboard

import android.os.Bundle
import android.widget.Toast
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.mgarguerr.ems.databinding.ActivityDashboardBinding
import com.mgarguerr.ems.domain.model.EnergyLiveData
import com.mgarguerr.ems.extensions.orZero
import com.mgarguerr.ems.presentation.core.BaseActivity
import javax.inject.Inject

class DashboardActivity : BaseActivity<ActivityDashboardBinding>(), DashboardViewContract {

    @Inject
    lateinit var presenter: DashboardPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onViewReady()
    }

    override fun provideBinding() = ActivityDashboardBinding.inflate(layoutInflater)

    //region VIEW CONTRACT
    override fun showWidgetQuasarsEnergy(energy: Float) {
        val values = listOf(
            BarEntry(0F, floatArrayOf(energy.takeIf { it < 0 }.orZero(), energy.takeIf { it > 0 }.orZero()))
        )
        binding.chartQuasars.data = BarData(BarDataSet(values, ""))
    }

    override fun showWidgetAllLiveData(energyLiveData: EnergyLiveData) {
        val values = listOf(
            BarEntry(0F, energyLiveData.solarPower),
            BarEntry(1F, energyLiveData.quasarsPower),
            BarEntry(2F, energyLiveData.gridPower),
            BarEntry(3F, energyLiveData.buildingDemand)
        )
        binding.chartLiveData.data = BarData(BarDataSet(values, ""))
    }

    override fun showWidgetStatistics() {
        // TODO: Ticket-XXXX. Implement statistics widget.
    }

    override fun showLoading() {
        Toast.makeText(this, "Show loader, not implemented", Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        Toast.makeText(this, "Hide loader, not implemented", Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        Toast.makeText(this, "Show error, not implemented", Toast.LENGTH_SHORT).show()
    }

    override fun hideError() {
        Toast.makeText(this, "Hide error, not implemented", Toast.LENGTH_SHORT).show()
    }
    //endregion
}
