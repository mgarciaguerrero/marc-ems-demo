package com.mgarguerr.ems.presentation.core.executors.di

import com.mgarguerr.ems.presentation.core.executors.Dispatchers
import com.mgarguerr.ems.presentation.core.executors.DispatchersProvider
import dagger.Binds
import dagger.Module

@Module
abstract class DispatchersModule {

    @Binds
    abstract fun bindDispatchers(dispatchersProvider: DispatchersProvider): Dispatchers
}
