package com.mgarguerr.ems.presentation.core

import kotlinx.coroutines.CoroutineScope

abstract class BasePresenter(coroutineScope: CoroutineScope) : CoroutineScope by coroutineScope
