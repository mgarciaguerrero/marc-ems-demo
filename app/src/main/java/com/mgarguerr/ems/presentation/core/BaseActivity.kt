package com.mgarguerr.ems.presentation.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

abstract class BaseActivity<T : ViewBinding> : AppCompatActivity(), HasAndroidInjector {

    val binding by lazy(::provideBinding)

    @Inject
    lateinit var injector: DispatchingAndroidInjector<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

    abstract fun provideBinding(): T

    override fun androidInjector(): AndroidInjector<Any> = injector
}
