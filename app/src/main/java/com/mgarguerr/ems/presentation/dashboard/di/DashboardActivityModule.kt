package com.mgarguerr.ems.presentation.dashboard.di

import androidx.lifecycle.lifecycleScope
import com.mgarguerr.ems.presentation.core.executors.di.DispatchersModule
import com.mgarguerr.ems.presentation.dashboard.DashboardActivity
import com.mgarguerr.ems.presentation.dashboard.DashboardViewContract
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope

@Module(includes = [DispatchersModule::class])
abstract class DashboardActivityModule {

    companion object {

        @Provides
        fun provideCoroutineScope(activity: DashboardActivity): CoroutineScope {
            return activity.lifecycleScope
        }
    }

    @Binds
    abstract fun bindView(activity: DashboardActivity): DashboardViewContract
}
