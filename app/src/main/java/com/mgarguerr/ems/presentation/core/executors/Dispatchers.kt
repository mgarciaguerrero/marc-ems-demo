package com.mgarguerr.ems.presentation.core.executors

import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

interface Dispatchers {

    val default: CoroutineDispatcher

    val main: CoroutineDispatcher

    val io: CoroutineDispatcher
}

class DispatchersProvider @Inject constructor() : Dispatchers {

    override val default: CoroutineDispatcher = kotlinx.coroutines.Dispatchers.Default

    override val main: CoroutineDispatcher = kotlinx.coroutines.Dispatchers.Main

    override val io: CoroutineDispatcher = kotlinx.coroutines.Dispatchers.IO
}
