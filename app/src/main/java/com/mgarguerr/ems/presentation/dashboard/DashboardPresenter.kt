package com.mgarguerr.ems.presentation.dashboard

import com.mgarguerr.ems.domain.interactor.GetLiveDataUseCase
import com.mgarguerr.ems.domain.model.EnergyLiveData
import com.mgarguerr.ems.extensions.execute
import com.mgarguerr.ems.presentation.core.BasePresenter
import com.mgarguerr.ems.presentation.core.executors.Dispatchers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DashboardPresenter @Inject constructor(
    private val view: DashboardViewContract,
    private val getLiveData: GetLiveDataUseCase,
    private val dispatchers: Dispatchers,
    coroutineScope: CoroutineScope
) : BasePresenter(coroutineScope) {

    fun onViewReady() {
        view.showLoading()
        execute(
            onExecute = {
                val energyLiveData = withContext(dispatchers.default) {
                    getLiveData.invoke()
                }
                view.hideLoading()
                showWidgets(energyLiveData)
            },
            onError = {
                view.hideLoading()
                view.showError()
            }
        )
    }

    private fun showWidgets(energyLiveData: EnergyLiveData) {
        view.showWidgetQuasarsEnergy(energyLiveData.quasarsPower)
        view.showWidgetAllLiveData(energyLiveData)
    }
}
