package com.mgarguerr.ems.presentation.dashboard

import com.mgarguerr.ems.domain.model.EnergyLiveData

interface DashboardViewContract {

    //region WIDGET
    fun showWidgetQuasarsEnergy(energy: Float)
    fun showWidgetAllLiveData(energyLiveData: EnergyLiveData)
    fun showWidgetStatistics()
    //endregion

    //region LOADING
    fun showLoading()
    fun hideLoading()
    //endregion

    //region ERROR
    fun showError()
    fun hideError()
    //endregion
}
