package com.mgarguerr.ems.di

import com.mgarguerr.ems.di.scope.ActivityScope
import com.mgarguerr.ems.presentation.dashboard.DashboardActivity
import com.mgarguerr.ems.presentation.dashboard.di.DashboardActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [DashboardActivityModule::class])
    abstract fun bindDashboardActivity(): DashboardActivity
}
