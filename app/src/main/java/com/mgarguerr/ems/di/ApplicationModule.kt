package com.mgarguerr.ems.di

import android.app.Application
import android.content.Context
import com.mgarguerr.ems.di.qualifiers.ForApplication
import com.mgarguerr.ems.di.scope.ApplicationScope
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    @ApplicationScope
    @ForApplication
    abstract fun provideApplicationContext(application: Application): Context
}
