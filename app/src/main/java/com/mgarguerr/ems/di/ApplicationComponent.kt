package com.mgarguerr.ems.di

import android.app.Application
import com.mgarguerr.ems.data.di.RepositoryModule
import com.mgarguerr.ems.di.scope.ApplicationScope
import com.mgarguerr.ems.presentation.EMSApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@ApplicationScope
@Component(
    modules = [
        ApplicationModule::class, AndroidInjectionModule::class, ActivityBuilderModule::class, RepositoryModule::class
    ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(application: EMSApplication)
}
