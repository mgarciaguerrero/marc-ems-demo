package com.mgarguerr.ems.extensions

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

fun CoroutineScope.execute(
    onExecute: suspend CoroutineScope.() -> Unit,
    onError: ((Throwable) -> Unit)
): Job {
    val errorHandler = CoroutineExceptionHandler { _, throwable ->
        onError.invoke(throwable)
    }

    return launch(errorHandler) {
        onExecute()
    }
}
