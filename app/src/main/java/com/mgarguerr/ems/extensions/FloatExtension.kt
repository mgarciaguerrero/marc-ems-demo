package com.mgarguerr.ems.extensions

fun Float?.orZero() = this ?: 0F
