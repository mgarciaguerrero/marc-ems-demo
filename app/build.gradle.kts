plugins {
    id("com.android.application")
    id("kotlin-android")
    kotlin("kapt")
}

android {
    compileSdk = Versions.BuildConfig.COMPILE_SDK

    defaultConfig {
        minSdk = Versions.BuildConfig.MIN_SDK
        targetSdk = Versions.BuildConfig.TARGET_SDK
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    // Android
    implementation(App.APP_COMPAT)
    implementation(App.CORE_KTX)
    implementation(App.CONSTRAINT_LAYOUT)
    implementation(App.MATERIAL)
    implementation(App.VIEW_BINDING)

    // Coroutines
    implementation(App.COROUTINES)

    //Lifecycle
    implementation(App.LIFECYCLE)
    kapt(App.LIFECYCLE_PROCESSOR)

    // DI
    implementation(App.DAGGER)
    implementation(App.DAGGER_ANDROID)
    kapt(App.DAGGER_COMPILER)
    kapt(App.DAGGER_PROCESSOR)

    // Network
    implementation(App.OKHTTP)
    implementation(App.RETROFIT)
    implementation(App.GSON)

    // Chart
    implementation(App.MV_ANDROID_CHART)

    // Testing
    testImplementation(Testing.KOTlIN_JUNIT)
    testImplementation(Testing.JUNIT4)
    testImplementation(Testing.MOCKK)
    testImplementation(Testing.COROUTINES_TEST)
}
