object Versions {

    object BuildConfig {

        const val MIN_SDK = 21
        const val TARGET_SDK = 31
        const val COMPILE_SDK = 31

        const val KOTLIN = "1.6.10"
        const val GRADLE = "7.0.4"
    }

    internal object Libraries {

        const val APP_COMPAT = "1.3.1"
        const val CONSTRAINT_LAYOUT = "2.1.0"
        const val CORE_KTX = "1.6.0"
        const val COROUTINES = "1.5.2"
        const val DAGGER = "2.40.5"
        const val GSON = "2.8.9"
        const val LIFECYCLE = "2.4.0"
        const val MATERIAL = "1.4.0"
        const val MV_ANDROID_CHART = "3.1.0"
        const val OKHTTP = "4.9.1"
        const val RETROFIT = "2.9.0"
        const val VIEW_BINDING = "7.0.4"
    }

    internal object Testing {

        const val JUNIT4 = "4.12"
        const val MOCKK = "1.12.1"
    }
}

object App {

    const val APP_COMPAT = "androidx.appcompat:appcompat:${Versions.Libraries.APP_COMPAT}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Versions.Libraries.CONSTRAINT_LAYOUT}"
    const val CORE_KTX = "androidx.core:core-ktx:${Versions.Libraries.CORE_KTX}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.Libraries.COROUTINES}"
    const val DAGGER = "com.google.dagger:dagger:${Versions.Libraries.DAGGER}"
    const val DAGGER_ANDROID = "com.google.dagger:dagger-android:${Versions.Libraries.DAGGER}"
    const val DAGGER_COMPILER = "com.google.dagger:dagger-compiler:${Versions.Libraries.DAGGER}"
    const val DAGGER_PROCESSOR = "com.google.dagger:dagger-android-processor:${Versions.Libraries.DAGGER}"
    const val GSON = "com.google.code.gson:gson:${Versions.Libraries.GSON}"
    const val LIFECYCLE = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.Libraries.LIFECYCLE}"
    const val LIFECYCLE_PROCESSOR = "androidx.lifecycle:lifecycle-common-java8:${Versions.Libraries.LIFECYCLE}"
    const val MATERIAL = "com.google.android.material:material:${Versions.Libraries.MATERIAL}"
    const val MV_ANDROID_CHART = "com.github.PhilJay:MPAndroidChart:v${Versions.Libraries.MV_ANDROID_CHART}"
    const val OKHTTP = "com.squareup.okhttp3:okhttp:${Versions.Libraries.OKHTTP}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.Libraries.RETROFIT}"
    const val VIEW_BINDING = "androidx.databinding:viewbinding:${Versions.Libraries.VIEW_BINDING}"
}

object Testing {

    const val KOTlIN_JUNIT = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.BuildConfig.KOTLIN}"
    const val JUNIT4 = "junit:junit:${Versions.Testing.JUNIT4}"
    const val MOCKK = "io.mockk:mockk:${Versions.Testing.MOCKK}"
    const val COROUTINES_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.Libraries.COROUTINES}"
}
