# ems-demo-project
Energy Management System demo android application written in kotlin.

### Architecture
This application is based on the [Clean Architecture](https://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html) principles and the [Model-View-Presenter](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) (MVP) architectural pattern.

### Technologies
The application uses the following libraries:

* [Dagger2](https://google.github.io/dagger/) - [dependency injection](https://en.wikipedia.org/wiki/Dependency_injection)
* [Coroutines](https://developer.android.com/kotlin/coroutines) - concurrency
* [Retrofit](http://square.github.io/retrofit/) - REST communication (Not used)
* [OkHttp](http://square.github.io/okhttp/) - HTTP client (Not used)
* [Gson](https://github.com/google/gson) - parsing JSON format
* [MPAndroidChart](https://github.com/PhilJay/MPAndroidChart) - charts
* [MockK](https://mockk.io/) - testing library

### Testing

The project includes **local unit tests** in all the layers following **BDD (Behavior Driven Development)** style.

### Assumptions

* No endpoint provided so I have assumed we should read the information from local files.
* Data type of attributes are not specified.
* Attributes are not optional.

### Nice to have
**1. Product**:
- Talk with the team to understand better the demo project
- Improve charts and complete the missing ones
- Implement the UI of loading and error states

**2. Technical**:
- Use modules per feature.
